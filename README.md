# UI Framework 기본 템플릿 제공

## 디바이스 중단점 (Device Breakpoint)
 - Mobile(`0 ~ 360px`) 기준 | `min-width : 320px`
 - Mobile(`361px ~ 480px`) 기준 |
 - Tablet(`481px ~ 760px`) 기준
 - Desktop(`761px ~ 960px`) 기준 |
 - Desktop(`961px ~ 1300px`) 기준 | `max-width : 1300px`

## 프로젝트 관리
 - Site Map : [보기](https://geulrara-ui-kit.gitlab.io/framework/)
 - UI Kit : [보기](https://geulrara-ui-kit.gitlab.io/framework/guide/ui-kit.html)
 - 공통영역 템플릿 : [보기](https://geulrara-ui-kit.gitlab.io/framework/guide/exmple.html)

## 프로젝트 일정 관리 (WBS)
 - WBS 문서 템플릿 : [보기](https://drive.google.com/drive/folders/1wMoUPV6DaunQuSELcJ1NOZUDEcPrByE2?usp=sharing)