// =========================================================
// SVG 포맷 처리 실행 (https://github.com/iconic/SVGInjector)
// =========================================================
(function(global, doc, $) {
  var svgInjection = function() {
    var mySVGsToInject = doc.querySelectorAll('img.inject-svg');
    var injectorOptions = {
        evalScripts: 'once', // always, once, never
        pngFallback: 'images/icon-png/', // PNG 대체 폴더 설정
    };
    SVGInjector(
        mySVGsToInject,
        injectorOptions
    );
  };
  svgInjection();
})(window, document, window.jQuery);

// =================================================================
// Scroll Control Plugin
// =================================================================
// 이동 시 스크롤 부드럽게
// License : https://github.com/cferdinandi/smooth-scroll
(function(global, $) {
  smoothScroll.init();
})(window, window.jQuery);


// =================================================================
// Popup Plugin
// =================================================================
// .popup - 팝업 버튼, 레이어 팝업 연결
(function(global, $) {
  var $popups = $('.popup').a11y_popup();
  // popup button class
  $('.btn-popup').on('click', function(e) {
    e.preventDefault();
    var popName = $(this).attr('data-id');
    var Wsize = $(window).width();
    // popup open layer (data-id)
    $.popupId($popups, popName).open();
  });
})(window, window.jQuery);

// =================================================================
// SelectBox Style
// =================================================================
(function(global, $) {
  var selectbox = $('select'),
      option_selected = $('select option[selected="selected"]');

  // 셀렉트박스 마우스 이벤트
  selectbox.click(function() {
    $(this).toggleClass('is-active');
  });

  // 셀렉트박스 키보드 이벤트
  selectbox.keydown(function(e) {
    if(e.keyCode == 13) {
      $(this).toggleClass("is-active");
    }
  });

  // 셀렉트박스 옵션 키보드 이벤트
  option_selected.keydown(function(e) {
    if(e.keyCode == 13) {
      $(this).toggleClass("is-active");
    }
  });

  // 셀렉트박스 옵션 이메일 속성 이벤트
  selectbox.change(function() {
    var $this = $(this),
        select_name = $this.children('option:selected').text();
    $this.siblings('label').children('input').val('')
      .attr('placeholder', select_name)
      .attr('aria-label', select_name);
  });

})(window, window.jQuery);

// =================================================================