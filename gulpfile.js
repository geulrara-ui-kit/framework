// ===================================================================
// NPM 모듈 호출
// ===================================================================
let exec = require('child_process').exec,
    gulp = require('gulp'),
    g_if = require('gulp-if'),
    rename = require('gulp-rename'),
    filter = require('gulp-filter'),
    fileinclude = require('gulp-file-include'),
    merge = require('merge-stream'),
    sass = require('gulp-sass'),
    sassInHtml = require('gulp-sass-in-html'),
    gcmq = require('gulp-group-css-media-queries'),
    autoprefixer = require('gulp-autoprefixer');
    sourcemaps = require('gulp-sourcemaps'),
    csso = require('gulp-csso'),
    spritesmith = require('gulp.spritesmith-multi'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    iconic = require('gulp-iconic'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

// ===================================================================
// 환경설정
// ===================================================================
// 디렉토리 설정
const SRC = 'markup';
const BUILD = 'public';

// 파일 압축 설정
var compress = {
  'image': false,
  'css': false,
  'js': false,
};

// 디렉토리 설정
var dir = {
  'css': SRC + '/assets/css',
  'css_min': BUILD + '/assets/css',
  'js': SRC + '/assets/js',
};

// 인크루드 할 파일 경로 이동
var htmlOrder = [
  // SRC + '/front/**/*.html',
  SRC + '/**/*.html',
]

// 자바스크립트 프래임워크(framework) & 라이브러리(Library) 병합
var jsOrder = [
  dir.js + '/jquery.tabs.js',
  dir.js + '/jquery.accordion.js',
  dir.js + '/jquery.a11y.popup.js',
  dir.js + '/jquery.carousel.js',
  dir.js + '/smooth-scroll.js',
  dir.js + '/svg-injector.min.js',
];

// 자바스크립트 공통 유지관리 js 파일 이동
var moveJS = [
  dir.js + '/jquery-3.3.1.min.js',
  dir.js + '/common.js',
  dir.js + '/imagesloaded.pkgd.min.js',
  dir.js + '/jquery.mCustomScrollbar.concat.min.js',
];

// 스타일 프래임워크(framework) 병합
var moveCSS = [
  SRC + '/sass/**.{sass,scss}', // sass 모든 스타일
];

// 스타일 압축 파일 이동
var minCSS = [
  dir.css_min + '/common.css', // Common UI
  dir.css_min + '/webfont.css', // Webfont
  dir.css_min + '/iy-ui-kit.css', // UI KIT
  dir.css_min + '/iy-guide.css', // UI KIT Guide
];

// 스프라이트 이미지 설정
var spritePath = {
  // 입력 경로 설정
  input: SRC + '/assets/images/sprite/**/*.png',
  // 출력 경로 설정
  output: {
    // 출력 파일 경로
    file: SRC + '/sass/sprite',
    // 출력 이미지 경로
    image: BUILD + '/assets/images/sprite'
  },
  // 생성된 Sass 파일의 이미지 경로 설정
  imgPath: '../images/sprite'
};

// ===================================================================
// 기본 업무
// ===================================================================
gulp.task('default', ['remove', 'server', 'font:move', 'favicon:move']);

// ===================================================================
// 빌드 업무
// ===================================================================
gulp.task('build', function () {
  compress.css = true;
  compress.js = true;
  compress.image = true;
  gulp.start('remove');
  gulp.start('fileinclude');
  gulp.start('js');
  gulp.start('sprites');
  gulp.start('imagemin');
  gulp.start('iconfont');
  gulp.start('iconfont:move');
  gulp.start('font:move');
  setTimeout(function () {
    gulp.start('sass');
    gulp.start('css:moveCSS');
    gulp.start('css:min');
  }, 5000);
});

// ===================================================================
// 관찰 업무
// ===================================================================
gulp.task('watch', function () {
  gulp.watch(SRC + '/**/*.html', ['fileinclude']);
  gulp.watch(SRC + '/sass/**/*', ['sass']);
  gulp.watch(SRC + '/assets/js/**/*', ['js']);
  gulp.watch(SRC + '/iconfont/fonts_here/**/*', ['iconfont']);
  gulp.watch(SRC + '/assets/fonts', ['font:move']);
  gulp.watch(SRC + '/assets/images/**/*', ['imagemin']);
  gulp.watch(SRC + '/assets/images/sprite', ['sprites']);
  gulp.watch(SRC + '/**/*.html').on('change', reload);
});

// ===================================================================
// 폴더 제거 업무
// ===================================================================
gulp.task('remove', function (cb) {
  let command = `rm -rf yarn-error.log debug.log npm-debug.log ${BUILD} ${BUILD}/assets/css/map ${BUILD}/assets/images/sprite ${SRC}/sass/sprite/_sprite-*.sass ${SRC}/iconfont/fonts ${SRC}/iconfont/fonts`;

  exec(command, function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

// ===================================================================
// 서버 업무
// ===================================================================
gulp.task('server', ['fileinclude', 'sass', 'js', 'imagemin', 'sprites', 'iconfont'], function () {
  browserSync.init({
    // 알림 설정
    notify: !true,
    // 포트 설정
    port: 8080,
    // 서버 설정
    server: {
      // 기본 디렉토리 설정
      baseDir: [BUILD],
      // 라우트 설정
      routes: {
        '/bower_components': 'bower_components',
      }
    },
  });
  gulp.start('watch');
});

// ===================================================================
// Include(Gulp File Include) 업무
// ===================================================================
// 로컬서버 설정하여 인크루드 사용
// [참고] https://www.npmjs.com/package/gulp-file-include
// [참고] https://www.npmjs.com/package/gulp-sass-in-html
gulp.task('fileinclude', function () {
  gulp.src(htmlOrder)
    .pipe(fileinclude({
      prefix: '@@',
    }))
    .pipe(sassInHtml())
    .pipe(gulp.dest(BUILD + '/'));
});


// ===================================================================
// Sass 업무
// ===================================================================
gulp.task('sass', function () {
  return gulp.src(moveCSS)
    .pipe(sourcemaps.init())
    .pipe(sass({
      // outputStyle: 'compressed',
      outputStyle: 'compact',
      includePaths: ['node_modules/susy/sass']
    }).on('error', sass.logError))
    .pipe(sourcemaps.init())
    .pipe(autoprefixer(
      'last 2 versions',
      'not ie < 11',
      'opera 12.1',
      'ios 6',
      'android 4'
    ))
    .pipe(sourcemaps.write('./map'))
    .pipe(gulp.dest(BUILD + '/assets/css'))
    .pipe(filter("**/*.css"))
    .pipe(gcmq())
    .pipe(reload({
      stream: true
    }));
});

// 공통 CSS 압축 파일 이동
gulp.task('css:min', function () {
  gulp.src(minCSS)
    .pipe(csso())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(BUILD + '/assets/css'));
});

// ===================================================================
// JS 병합 업무
// ===================================================================
gulp.task('js', ['js:concat']);

// 공통 JS  파일 이동
gulp.task('js:moveJS', function () {
  gulp.src(moveJS)
    .pipe(gulp.dest(BUILD + '/assets/js'));
});

// 공통 JS 파일 병합 후 이동
gulp.task('js:concat', ['js:moveJS'], function () {
  gulp.src(jsOrder)
    .pipe(concat('js-lib.js'))
    .pipe(g_if(compress.js, uglify()))
    .pipe(g_if(compress.js, rename('js-lib.min.js')))
    .pipe(gulp.dest(BUILD + '/assets/js'));
});

// ===================================================================
// Images min 업무
// ===================================================================
gulp.task('imagemin', function () {
  return gulp.src(SRC + '/assets/images/**/*')
    .pipe(
      g_if(compress.image,
        imagemin({
          progressive: true,
          svgoPlugins: [{
            removeViewBox: false
          }],
          use: [pngquant()]
        })
      )
    )
    .pipe(gulp.dest(BUILD + '/assets/images'));
});

// ===================================================================
// Sprites images 업무
// ===================================================================
gulp.task('sprites', function () {
  var opts = {
    spritesmith: function (options, sprite, icons) {
      options.imgPath = `${spritePath.imgPath}/${options.imgName}`;
      options.cssName = `_sprite-${sprite}.sass`;
      options.cssTemplate = 'sass.template.handlebars';
      options.cssSpritesheetName = sprite;
      options.padding = 5;
      options.cssVarMap = function (sp) {
        sp.name = `${sprite}-${sp.name}`;
      };
      return options;
    }
  };

  var spriteData = gulp.src(spritePath.input).pipe(spritesmith(opts)).on('error', function (err) {
    console.log(err)
  });

  var imgStream = spriteData.img.pipe(gulp.dest(spritePath.output.image));
  var cssStream = spriteData.css.pipe(gulp.dest(spritePath.output.file));

  return merge(imgStream, cssStream);
});

// ===================================================================
// Iconfont 업무
// ===================================================================
gulp.task('iconfont', ['iconfont:make']);

gulp.task('iconfont:make', function (cb) {
  iconic({
    // 템플릿 파일 경로 설정 (filename)
    // gulp-iconic/template/_iconfont.scss
    cssTemplate: SRC + '/sass/lib/_iconfont.scss',
    // Scss 생성 파일 경로 설정
    cssFolder: SRC + '/sass/uikit/',
    // Fonts 생성 파일 경로 설정
    fontFolder: SRC + '/iconfont/fonts',
    // SVG 파일 경로 설정
    svgFolder: SRC + '/iconfont/fonts_here',
    // Preview 생성 폴더 경로 설정
    previewFolder: SRC + '/iconfont/preview',
    // font 경로 설정
    fontUrl: '/assets/fonts',
    // 아이콘 베이스라인 위치 설정
    descent: 80
  }, cb);
  setTimeout(function () {
    gulp.start('iconfont:move');
  }, 5000);
});

gulp.task('iconfont:move', function () {
  gulp.src(SRC + '/iconfont/fonts/*')
    .pipe(gulp.dest(BUILD + '/assets/fonts'));
});

// ===================================================================
// web font 이동 업무
// ===================================================================
gulp.task('font:move', function () {
  gulp.src(SRC + '/assets/fonts/**/*')
    .pipe(gulp.dest(BUILD + '/assets/fonts'));
});

// ===================================================================
// Favicon 이동 업무
// ===================================================================
gulp.task('favicon:move', function () {
  gulp.src(SRC + '/assets/favicon/**/*')
    .pipe(gulp.dest(BUILD + '/assets/favicon'));
});